﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace CentralRepo.Network
{
    public class ReplicationAPI
    {
        private IConfiguration _config;

        public ReplicationAPI(IConfiguration config) {
            this._config = config;
        }
        public bool RequestInfringements(String json)
        {
            //var url = config.GetSection("").ToString();
            var url = _config.GetSection("UrlReplication:Prod").Value;
            //var url = "https://qs-application.mx/WS/MobileProduction/Qsistemas/PAPMobile.asmx/InfringementReception";

            string data = "json=" + json;
            try
            {
                using (WebClient wc = new WebClient())
                {
                    wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    string response = wc.UploadString(url, data);
                    var myObject = JObject.Parse(response);
                    return Convert.ToBoolean(myObject["Success"].SelectToken("Success"));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return false;
            }
        }
    }
}
