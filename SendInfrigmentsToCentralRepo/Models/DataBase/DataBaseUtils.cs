﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace SendInfrigmentsToCentralRepo.DataBase
{
    class DataBaseUtils
    {
        private string conString;
        public DataBaseUtils(string conString) {
            this.conString = conString;
        }
        public DataSet SelectData(string query, string tableName)
        {
            try
            {
                DataSet ds = new DataSet();
                using (var con = new SqlConnection(conString))
                {
                    using (var cmd = con.CreateCommand())
                    {
                        con.Open();
                        cmd.CommandText = query;
                        using (SqlDataAdapter adt = new SqlDataAdapter(cmd))
                        {
                            adt.Fill(ds, tableName);
                        }
                    }
                }
                return ds;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return null;
            }
        }
        public bool ExecuteProcedure(List<SqlParameter> pStore, string pName) {
            try
            {
                using (var con = new SqlConnection(conString)) {
                    var command = con.CreateCommand();
                    con.Open();
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = pName;

                    foreach (SqlParameter pSql in pStore) {
                        command.Parameters.AddWithValue(pSql.ParameterName, pSql.Value);
                    }
                    command.ExecuteNonQuery();
                }
                return true;
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message.ToString());
                return false;
            }
        }
    }
}
