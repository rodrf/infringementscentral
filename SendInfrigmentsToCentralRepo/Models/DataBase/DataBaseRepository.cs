﻿using CentralRepo.Models.Network;
using CentralRepo.Models.Network.Response;
using CentralRepo.Network;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Text;

namespace SendInfrigmentsToCentralRepo.DataBase
{
    public class DataBaseRepository : IDatabaseUtils
    {
        private IConfiguration _config;
        private String conString;
        public DataBaseRepository(string conString, IConfiguration config) {
            this.conString = conString;
            this._config = config;
        }
        public int sendInfringementsToCentral() {
            DataBaseUtils db = new DataBaseUtils(conString);//config.GetConnectionString("CON_QS")); //Server=tcp:189.240.246.19:443;Database=SIIP_QSISTEMAS;Trusted_Connection=True;MultipleActiveResultSets=true
            DataSet ds = new DataSet();
            string query = "EXEC SIIPPA_INFRACCION_ENVIO_CENTRAL @Statement = 'select_pending'";
            StringBuilder sbWhereIN = new StringBuilder("(");
            string sbWhereLast;
            //Para armar el Json 
            InfringementsRequest request;
            Articles artFracc;
            int errorCode = 0;
            try
            {
                //Obtenemos las infracciones pendientes por enviar
                ds = db.SelectData(query, "PENDIENTES");
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        //Se arma (1,2,3,
                        sbWhereIN.Append(item[0] + ",");
                    }
                    //Removemos la última coma y agregar ")"
                    sbWhereLast = sbWhereIN.ToString().Remove(sbWhereIN.Length - 1, 1) + ")";
                    var queryInfringements = getInfringementQry() + sbWhereLast;
                    //Obtenemos el listado de infracciones nuevas
                    var dsInfra = db.SelectData(queryInfringements, "INF_TO_SEND");
                    if (dsInfra != null && dsInfra.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow infraItem in dsInfra.Tables[0].Rows)
                        {
                            request = new InfringementsRequest();
                            request.Day = infraItem["Day"].ToString();
                            request.IdState = Convert.ToInt32(infraItem["IdState"]);
                            request.IdTown = Convert.ToInt32(infraItem["IdTown"]);
                            request.TicketNumber = infraItem["TicketNumber"].ToString();
                            request.IsPay = Convert.ToInt32(infraItem["IsPay"]) == 1 ? true : false;
                            request.IdentifyingDocument = infraItem["IdentifyingDocument"].ToString();
                            request.IdInfringement = Convert.ToInt64(infraItem["IdInfringement"]);
                            request.Address = infraItem["address"].ToString();

                            //Obteneos los artículos y fracciones por cada infracción.
                            var idInfringement = infraItem["IdInfringement"];
                            var dsArt = db.SelectData(getArticlesFraccQry(Convert.ToInt64(idInfringement)), "ARTICLES");
                            if (dsArt != null && dsArt.Tables[0].Rows.Count > 0)
                            {
                                foreach (DataRow artItem in dsArt.Tables[0].Rows)
                                {
                                    artFracc = new Articles();
                                    artFracc.Art = artItem["Art"].ToString();
                                    artFracc.Fracc = artItem["Fracc"].ToString();
                                    artFracc.Description = artItem["Description"].ToString();
                                    artFracc.Uma = Convert.ToDouble(artItem["Uma"]);
                                    request.Fundament.Add(artFracc);
                                }
                            }
                            var resJson = Newtonsoft.Json.JsonConvert.SerializeObject(request);
                            //Consumir servicio para enviar cada infracción
                            if (new ReplicationAPI(_config).RequestInfringements(resJson))
                            {
                                if (updateInfringement(Convert.ToInt64(idInfringement), conString))
                                {
                                    errorCode= 0; //Success
                                }
                                else
                                {
                                    errorCode= 1; //local update error
                                }
                            }
                            else
                            {
                                errorCode= 2; // Service Error
                            }
                        }
                    }
                    else
                    {
                        errorCode= 3; // No infringements were found with that id
                    }
                }
                else {
                    errorCode= 4; // No pendings
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                errorCode = 10; //Error general
            }
            return errorCode;
        }
        private bool updateInfringement(long idInfringement, string conString) {
            DataBaseUtils db = new DataBaseUtils(conString);
            try
            {
                var query = "EXEC SIIPPA_INFRACCION_ENVIO_CENTRAL @Statement ='update_pending', @ID_INFRACCION = "+idInfringement.ToString();
                List<SqlParameter> lsParams = new List<SqlParameter>();
                lsParams.Add(new SqlParameter("@Statement", "update_pending"));
                lsParams.Add(new SqlParameter("@ID_INFRACCION", idInfringement.ToString()));

                return db.ExecuteProcedure(lsParams, "SIIPPA_INFRACCION_ENVIO_CENTRAL");
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message.ToString());
                return false;
            }
        }

       
        public String getInfringementQry() {
            return "SELECT " +
                "CONVERT(varchar, INFRA.REGISTRO_FECHA, 103)+' '+CONVERT(varchar, INFRA.REGISTRO_FECHA, 24) AS Day," +
                "(SELECT INFRACCION_ID_ENTIDAD FROM SIIPTC_CONFIGURACION) AS IdState," +
                "(SELECT INFRACCION_ID_MUNICIPIO FROM SIIPTC_CONFIGURACION) AS IdTown," +
                "INFRA.FOLIO AS TicketNumber," +
                "INFRA.BAN_PAGADA AS IsPay," +
                "IN_VE.NUM_DOCUMENTO_IDENTIFICADOR AS IdentifyingDocument," +
                "INFRA.ID_INFRACCION AS IdInfringement, " +
                "ISNULL((CALLE.CALLE +', '+ COL.COLONIA), '') AS address " +
                "FROM SIIPTA_INFRACCION INFRA " +
                "INNER JOIN SIIPTA_INFRACCION_INFRACCION_VEHICULO IN_IN_VE ON INFRA.ID_INFRACCION = IN_IN_VE.ID_INFRACCION " +
                "INNER JOIN SIIPTA_INFRACCION_VEHICULO IN_VE ON IN_IN_VE.ID_VEHICULO = IN_VE.ID_VEHICULO " +
                "LEFT JOIN SIIPTA_DIRECCION_INFRACCION DIR_INFRA ON INFRA.ID_INFRACCION = DIR_INFRA.ID_INFRACCION " +
                "LEFT JOIN SIIPTA_DIRECCION DIR ON DIR_INFRA.ID_DIRECCION = DIR.ID_DIRECCION " +
                "LEFT JOIN SIIPTC_CALLE CALLE ON DIR.ID_CALLE = CALLE.ID_CALLE " +
                "LEFT JOIN SIIPTC_COLONIA COL ON DIR.ID_COLONIA = COL.ID_COLONIA " +
                "WHERE INFRA.ID_INFRACCION IN ";
        }
        public String getArticlesFraccQry(long idInfringement) {
            return "SELECT " +
                "ARTICULO.ARTICULO AS Art," +
                "FRACCION.FRACCION AS Fracc," +
                "FRACCION.DESCRIPCION AS Description," +
                "FRACCION.SALARIOS_MINIMO AS Uma," +
                "INFRA_FRA.ID_INFRACCION " +
                "FROM SIIPTA_INFRACCION_FRACCION INFRA_FRA " +
                "INNER JOIN SIIPTC_INFRACCION_FRACCION FRACCION ON INFRA_FRA.ID_FRACCION = FRACCION.ID " +
                "INNER JOIN SIIPTC_INFRACCION_ARTICULO ARTICULO ON FRACCION.ID_ARTICULO = ARTICULO.ID " +
                "WHERE INFRA_FRA.ID_INFRACCION ="+idInfringement.ToString();
        }
    }
}
