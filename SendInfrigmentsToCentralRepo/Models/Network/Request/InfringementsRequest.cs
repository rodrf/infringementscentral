﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CentralRepo.Models.Network
{
    public class InfringementsRequest
    {
        public string Day { get; set; }
        public int IdState { get; set; }
        public int IdTown { get; set; }
        public string TicketNumber { get; set; }
        public bool IsPay { get; set; }
        public string IdentifyingDocument { get; set; }
        public List<Articles> Fundament { get; set; } = new List<Articles>();
        public long IdInfringement { get; set; }
        public string Address { get; set; }

    }
    public class Articles { 
        public string Art { get; set; }
        public string Fracc { get; set; }
        public string Description { get; set; }
        public double Uma { get; set; }

    }
}
