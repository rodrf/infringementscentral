﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CentralRepo.Models.Network.Response
{
    public class Success
    {
        //public bool Success { get; set; }
        public int ErrorCode { get; set; }
        public String Message { get; set; }
        public InfringementData Datos { get; set; } = new InfringementData();
    }
    public class InfringementData {
        public bool SUCCESS { get; set; }
        public int ERROR_CODE { get; set; }
        public String ERROR_MESSAGE { get; set; }
        public Array DATA { get; set; }

    }
}
