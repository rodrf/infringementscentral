using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CentralRepo.Workers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging.EventLog;

namespace SendInfrigmentsToCentralRepo
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseWindowsService()
                .UseSystemd()
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<WQsistemas>()
                    .Configure<EventLogSettings>(config =>{
                        config.LogName = "SIIP_QSISTEMAS";
                        config.SourceName = "Qsistemas Replication";
                    });
                    services.AddHostedService<WAtlacomulco>()
                    .Configure<EventLogSettings>(config =>
                    {
                        config.LogName = "SIIP_ATLACOMULCO";
                        config.SourceName = "Atlacomulco Replication";
                    });
                    //services.AddHostedService<WIxtlahuaca>()
                    //.Configure<EventLogSettings>(config =>
                    //{
                    //    config.LogName = "SIIP_IXTLAHUACA";
                    //    config.SourceName = "Ixtlahuaca Replication";
                    //});
                    //services.AddHostedService<WNezahualcoyotl>()
                    //.Configure<EventLogSettings>(config =>
                    //{
                    //    config.LogName = "SIIP_NEZAHUALCOYOTL";
                    //    config.SourceName = "Nezahualc�yotl Replication";
                    //});
                    //services.AddHostedService<WNicolasRomero>()
                    //.Configure<EventLogSettings>(config =>
                    //{
                    //    config.LogName = "SIIP_NICOLAS_ROMERO";
                    //    config.SourceName = "Nicol�s Romero Replication";
                    //});
                    //services.AddHostedService<WOtzolotepec>()
                    //.Configure<EventLogSettings>(config =>
                    //{
                    //    config.LogName = "SIIP_OTZOLOTEPEC";
                    //    config.SourceName = "Otzolotepec Replication";
                    //});
                    //services.AddHostedService<WSanMateoAtenco>()
                    //.Configure<EventLogSettings>(config =>
                    //{
                    //    config.LogName = "SIIP_SANMATEOATENCO";
                    //    config.SourceName = "San Mateo Atenco Replication";
                    //});
                    ////services.AddHostedService<WAtlacomulco>()
                    ////.Configure<EventLogSettings>(config =>
                    ////{
                    ////    config.LogName = "SIIP_TECAMAC";
                    ////    config.SourceName = "Tecamac Replication";
                    ////});
                    //services.AddHostedService<WTemoaya>()
                    //.Configure<EventLogSettings>(config =>
                    //{
                    //    config.LogName = "SIIP_TEMOAYA";
                    //    config.SourceName = "Temoaya Replication";
                    //});
                    //services.AddHostedService<WTenancingo>()
                    //.Configure<EventLogSettings>(config =>
                    //{
                    //    config.LogName = "SIIP_TENANCINGO";
                    //    config.SourceName = "Tenancingo Replication";                    
                    //});
                    //services.AddHostedService<WTepotzotlan>()
                    //.Configure<EventLogSettings>(config =>
                    //{
                    //    config.LogName = "SIIP_TEPOTZOTLAN";
                    //    config.SourceName = "Tepotzotlan Replication";
                    //});
                    ////services.AddHostedService<WAtlacomulco>()
                    ////.Configure<EventLogSettings>(config =>
                    ////{
                    ////    config.LogName = "SIIP_TEXCOCO";
                    ////    config.SourceName = "Otzolotepec Replication";
                    ////});
                    //services.AddHostedService<WTianguistenco>()
                    //.Configure<EventLogSettings>(config =>
                    //{
                    //    config.LogName = "SIIP_TIANGUISTENCO";
                    //    config.SourceName = "Tianguistenco Replication";
                    //});
                    services.AddHostedService<WZinacantepec>()
                    .Configure<EventLogSettings>(config =>
                    {
                        config.LogName = "SIIP_ZINACANTEPEC";
                        config.SourceName = "Zinacantepec Replication";
                    });

                }).UseWindowsService();
    }
}
