using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CentralRepo.Utils;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SendInfrigmentsToCentralRepo.DataBase;

namespace SendInfrigmentsToCentralRepo
{
    public class WQsistemas : BackgroundService
    {
        private readonly ILogger<WQsistemas> _logger;
        private readonly IConfiguration _config;
        private static readonly string Township = "SIIP_QSISTEMAS";

        public WQsistemas(ILogger<WQsistemas> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
        }

        public override async Task StartAsync(CancellationToken cancellationToken) {
            _logger.LogInformation(Township+" Replication start");
            await base.StartAsync(cancellationToken);
        }
        public override async Task StopAsync(CancellationToken cancellationToken) {
            _logger.LogInformation(Township + " Replication stop");
            await base.StopAsync(cancellationToken);
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                DataBaseRepository dbRepo = new DataBaseRepository(_config.GetConnectionString(Township), _config);
                var res = dbRepo.sendInfringementsToCentral();
                switch (res) { 
                    case 0: //Success
                        _logger.LogInformation(Township+" running at: {time}", DateTimeOffset.Now);
                        break;
                    case 1: // Local update error
                        _logger.LogCritical(Township + " local update error at: {time}", DateTimeOffset.Now);
                        break;
                    case 2: //WService error
                        _logger.LogError(Township + " WebService error at: {time}", DateTimeOffset.Now);
                        break;
                    case 3: //No new infrigments
                        _logger.LogError(Township + " no infringements were found with that id: {time}", DateTimeOffset.Now);
                        break;
                    case 4: //No pendings
                        _logger.LogInformation(Township + " no pendings to send at: {time}", DateTimeOffset.Now);
                        break;
                     default:
                        _logger.LogCritical(Township + " general error at: {time}", DateTimeOffset.Now);
                        break;
                }
                await Task.Delay(Constants.Timer.Delay);
            }
        }
    }
}
